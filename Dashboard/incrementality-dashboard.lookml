# Put me in maa_incrementality/models/wayfair.model.lkml:22
aggregate_table: rollup__cohort_users_group__days_to_conversion {
  query: {
    dimensions: [cohort_users.group, days_to_conversion]
    measures: [conversions]
    timezone: "America/New_York"
  }

  materialization: {
    persist_for: "24 hours"
  }
}

# Put me in maa_incrementality/models/maa_incrementality.model.lkml:13
aggregate_table: rollup__campaigntimestamp_date__campaigntimestamp_month {
  query: {
    dimensions: [campaigntimestamp_date, campaigntimestamp_month]
    measures: [count_control_users, count_treatment_users]
    timezone: "America/New_York"
  }

  materialization: {
    datagroup_trigger: maa_incrementality_default_datagroup
  }
}

# Put me in maa_incrementality/models/maa_incrementality.model.lkml:13
aggregate_table: rollup__campaigntimestamp_month__conversiontimestamp_date {
  query: {
    dimensions: [campaigntimestamp_month, conversiontimestamp_date]
    measures: [count_control_users, count_treatment_users]
    filters: [prod_wayfair_fact_incrementality.analyticscaptureid: "-NULL"]
    timezone: "America/New_York"
  }

  materialization: {
    datagroup_trigger: maa_incrementality_default_datagroup
  }
}

# Put me in maa_incrementality/models/maa_incrementality.model.lkml:13
aggregate_table: rollup__campaigntimestamp_month__group {
  query: {
    dimensions: [campaigntimestamp_month, group]
    measures: [count_users]
    timezone: "America/New_York"
  }

  materialization: {
    datagroup_trigger: maa_incrementality_default_datagroup
  }
}

# Put me in maa_incrementality/models/maa_incrementality.model.lkml:13
aggregate_table: rollup__campaigntimestamp_month {
  query: {
    dimensions: [campaigntimestamp_month]
    measures: [count_control_users, count_treatment_users, sum_control_value, sum_treatment_value]
    timezone: "America/New_York"
  }

  materialization: {
    datagroup_trigger: maa_incrementality_default_datagroup
  }
}
