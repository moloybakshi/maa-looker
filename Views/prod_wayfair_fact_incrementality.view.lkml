view: prod_wayfair_fact_incrementality {
  sql_table_name: incrementality.prod_wayfair_fact_incrementality ;;

  dimension: pk  {
    primary_key: yes
    sql: ${cohort}||'_'||${group} ;;
  }

  dimension: accountid {
    type: number
    value_format_name: id
    sql: ${TABLE}.accountid ;;
  }

  dimension: accountname {
    type: string
    sql: ${TABLE}.accountname ;;
  }

  dimension: agegroup {
    type: number
    sql: ${TABLE}.agegroup ;;
  }

  dimension: analyticscaptureid {
    type: string
    sql: ${TABLE}.analyticscaptureid ;;
  }

  dimension: audiencelist {
    type: string
    sql: ${TABLE}.audiencelist ;;
  }

  dimension: bidprice_usd {
    type: number
    sql: ${TABLE}.bidprice_usd ;;
  }

  dimension: campaigncountryid {
    type: number
    value_format_name: id
    sql: ${TABLE}.campaigncountryid ;;
  }

  dimension: campaigneventtype {
    type: string
    sql: ${TABLE}.campaigneventtype ;;
  }

  dimension: campaignid {
    type: number
    value_format_name: id
    sql: ${TABLE}.campaignid ;;
  }

  dimension: campaignname {
    type: string
    sql: ${TABLE}.campaignname ;;
  }

  dimension_group: campaigntimestamp {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.campaigntimestamp ;;
  }

  dimension: cohort {
    type: string
    sql: DATE_TRUNC('month', campaigntimestamp)::date ;;
  }

  dimension: card_type {
    type: string
    sql: ${TABLE}.card_type ;;
  }

  dimension: cohortid {
    type: string
    sql: ${TABLE}.cohortid ;;
  }

  dimension_group: conversiontimestamp {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.conversiontimestamp ;;
  }

  dimension: correlationid {
    type: string
    sql: ${TABLE}.correlationid ;;
  }

  dimension: customerbleedwithinmg {
    type: yesno
    sql: ${TABLE}.customerbleedwithinmg ;;
  }

  dimension: daysbetweengroups {
    type: number
    sql: ${TABLE}.daysbetweengroups ;;
  }

  dimension: dayssincelastevent {
    type: number
    sql: ${TABLE}.dayssincelastevent ;;
  }

  dimension: device {
    type: string
    sql: ${TABLE}.device ;;
  }

  dimension: emailhash {
    type: string
    sql: ${TABLE}.emailhash ;;
  }

  dimension: gender {
    type: string
    sql: ${TABLE}.gender ;;
  }

  dimension_group: lastseenonaccount {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.lastseenonaccount ;;
  }

  dimension: matchtype {
    type: string
    sql: ${TABLE}.matchtype ;;
  }

  dimension: measurementgroupid {
    type: number
    value_format_name: id
    sql: ${TABLE}.measurementgroupid ;;
  }

  dimension: measurementgroupname {
    type: string
    sql: ${TABLE}.measurementgroupname ;;
  }

  dimension: pagetype {
    type: string
    sql: ${TABLE}.pagetype ;;
  }

  dimension: partnerid {
    type: number
    value_format_name: id
    sql: ${TABLE}.partnerid ;;
  }

  dimension: partnername {
    type: string
    sql: ${TABLE}.partnername ;;
  }

  dimension: payment_type {
    type: string
    sql: ${TABLE}.payment_type ;;
  }

  dimension: persona {
    type: string
    sql: ${TABLE}.persona ;;
  }

  dimension: position {
    type: string
    sql: ${TABLE}.position ;;
  }

  dimension: previouslyexposedtoaccount {
    type: yesno
    sql: ${TABLE}.previouslyexposedtoaccount ;;
  }

  dimension: referralcreativeid {
    type: number
    value_format_name: id
    sql: ${TABLE}.referralcreativeid ;;
  }

  dimension: rowid {
    type: string
    sql: ${TABLE}.rowid ;;
  }

  dimension: sessionid {
    type: string
    sql: ${TABLE}.sessionid ;;
  }

  dimension: sub_persona {
    type: string
    sql: ${TABLE}.sub_persona ;;
  }

  dimension: subverticalname {
    type: string
    sql: ${TABLE}.subverticalname ;;
  }

  dimension: sum_value_percentile_rank {
    type: number
    sql: ${TABLE}.sum_value_percentile_rank ;;
  }

  dimension: targetgroupid {
    type: number
    value_format_name: id
    sql: ${TABLE}.targetgroupid ;;
  }

  dimension: targetgroupname {
    type: string
    sql: ${TABLE}.targetgroupname ;;
  }

  dimension: timeout {
    type: yesno
    sql: ${TABLE}.timeout ;;
  }

  dimension_group: timeswitch {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.timeswitch ;;
  }

  dimension: userbleedwithinmg {
    type: yesno
    sql: ${TABLE}.userbleedwithinmg ;;
  }

  dimension: validghostimpression {
    type: yesno
    sql: ${TABLE}.validghostimpression ;;
  }

  dimension: validuserpath {
    type: yesno
    sql: ${TABLE}.validuserpath ;;
  }

  dimension: value {
    type: number
    sql: ${TABLE}.value ;;
  }

  dimension: verticalname {
    type: string
    sql: ${TABLE}.verticalname ;;
  }

  dimension: widgetroktmltimedelta {
    type: number
    sql: ${TABLE}.widgetroktmltimedelta ;;
  }

  dimension: group {
    type: string
    sql: CASE WHEN ${campaigneventtype} = 'ghost_impression' THEN 'Control' ELSE 'Treatment' END;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  measure: count_users {
    type: count_distinct
    sql: ${emailhash} ;;
    drill_fields: [detail*]
  }

  measure: count_control_users {
    type: count_distinct
    sql: CASE WHEN ${group} = 'Control' THEN ${emailhash} END ;;
    drill_fields: [detail*]
  }

  measure: count_treatment_users {
    type: count_distinct
    sql: CASE WHEN ${group} <> 'Control' THEN ${emailhash} END ;;
    drill_fields: [detail*]
  }

  measure: sum_control_value {
    type: sum
    sql: CASE WHEN ${group} = 'Control' THEN ${value} END ;;
  }

  measure: sum_treatment_value {
    type: sum
    sql: CASE WHEN ${group} <> 'Control' THEN ${value} END ;;
  }

  dimension: days_to_conversion {
    type: number
    sql:CASE WHEN ${analyticscaptureid} IS NOT NULL then datediff('day', ${campaigntimestamp_date},${conversiontimestamp_date}) END ;;
  }

  measure: conversions {
    type: count_distinct
    sql: ${analyticscaptureid} ;;
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      verticalname,
      targetgroupname,
      subverticalname,
      partnername,
      measurementgroupname,
      campaignname,
      accountname
    ]
  }
}
