view: cohort_users {

derived_table: {
  sql: SELECT
      COUNT(DISTINCT emailhash) users
      , DATE_TRUNC('month', campaigntimestamp)::date cohort
      , CASE WHEN campaigneventtype = 'ghost_impression' THEN 'Control' else 'Treatment' END as group_type
      FROM
        incrementality.prod_wayfair_fact_incrementality
      GROUP BY
        cohort,group_type
    ;;
}

  dimension: pk  {
    primary_key: yes
    sql: ${cohort}||'_'||${group} ;;
  }

  dimension: cohort {
    type: string
    sql: ${TABLE}.cohort ;;
  }

  dimension: group {
    type: string
    sql:  ${TABLE}.group_type ;;
  }

  dimension: users {
    type: number
    sql:  ${TABLE}.users;;
  }

  measure: sum_users {
    type: sum
    sql: ${users} ;;
  }
}
