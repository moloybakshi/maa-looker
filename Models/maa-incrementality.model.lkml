connection: "redshift_prod"

# include all the views
include: "/views/**/*.view"

datagroup: maa_incrementality_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: maa_incrementality_default_datagroup

explore: prod_wayfair_fact_incrementality {}

# explore: activateplacement {}

# explore: all_session_devices {}

# explore: analyse_load_times_new_world_sg {}

# explore: analyse_load_times_new_world_sg_old {}

# explore: analysis_hcom_all_conversions {}

# explore: analysis_hcom_all_impressions {}

# explore: analysis_hcom_rolling_gbv {}

# explore: conversions_hcom_frnl_20190417 {}

# explore: conversions_kayo_20191024_20191028 {}

# explore: csv_orbitz_incrementality_gbv_conversions_20190829 {}

# explore: csv_orbitz_incrementality_gp_conversions_20190829 {}

# explore: csv_wotif_incrementality_gbv_conversions_20190829 {}

# explore: csv_wotif_incrementality_gp_conversions_20190829 {}

# explore: customuploadedconversions {}

# explore: data_enrichment_log {}

# explore: data_enrichment_log_moloy {}

# explore: data_enrichment_log_moloy2 {}

# explore: dev_blue_apron_fact_incrementality_full {}

# explore: dev_dominos_au_fact_incrementality {}

# explore: dev_hotels_nofixes_add_timedelta_gi_stream {}

# explore: dev_hotels_nofixes_fact_incrementality {}

# explore: dev_hotels_nofixes_fact_incrementality_backup {}

# explore: dev_hotels_nofixes_find_valid_measurement_group_ids {}

# explore: dev_hotels_nofixes_flag_userpath_impressions {}

# explore: dev_hotels_nofixes_flag_valid_ghost_impressions {}

# explore: dev_hotels_nofixes_ghost_impression_stream {}

# explore: dev_hotels_nofixes_incinc_fact_incrementality {}

# explore: dev_hotels_nofixes_incinc_fact_incrementality_backup {}

# explore: dev_hotels_nofixes_mobile_fact_incrementality {}

# explore: dev_hotels_nofixes_preprocess_table {}

# explore: dev_hotels_nofixes_test_add_uploaded_conversions {}

# explore: dev_hotels_nofixes_test_carbon_purchase_stream {}

# explore: dev_hotels_nofixes_test_fact_incrementality {}

# explore: dev_hotels_nofixes_test_fact_incrementality_backup {}

# explore: dev_hotels_nofixes_test_find_valid_measurement_group_ids {}

# explore: dev_hotels_nofixes_test_get_last_timestamps {}

# explore: dev_hotels_nofixes_test_preprocess_table {}

# explore: dev_hotels_nofixes_test_purchases_table {}

# explore: dev_hotels_nofixes_usd_usd_fix {}

# explore: dev_hulu_alex_concatenated_streams {}

# explore: dev_hulu_alex_fact_incrementality {}

# explore: dev_hulu_alex_fact_incrementality_backup {}

# explore: dev_hulu_alextest_events {}

# explore: dev_hulu_alextest_eventsemails {}

# explore: dev_hulu_alextest_eventsemails_dedupped {}

# explore: dev_hulu_bleedincinc_fact_incrementality {}

# explore: dev_hulu_bleedincinc_fact_incrementality_backup {}

# explore: dev_hulu_bleedincinc_preprocess_table {}

# explore: dev_hulu_bleedincinc_stable_fact_incrementality {}

# explore: dev_hulu_bleedincinc_stable_fact_incrementality_backup {}

# explore: dev_hulu_bleedincinc_stable_find_valid_measurement_group_ids {}

# explore: dev_hulu_bleedincinc_stable_preprocess_table {}

# explore: dev_hulu_fact_incrementality {}

# explore: dev_hulu_incinc_fact_incrementality {}

# explore: dev_hulu_incinc_fact_incrementality_backup {}

# explore: dev_hulu_incincfix_fact_incrementality {}

# explore: dev_hulu_incincfix_fact_incrementality_backup {}

# explore: dev_hulu_test1_add_uploaded_conversions {}

# explore: dev_hulu_test1_carbon_purchase_stream {}

# explore: dev_hulu_test1_find_valid_measurement_group_ids {}

# explore: dev_hulu_test1_purchases_table {}

# explore: dev_hulu_test_add_uploaded_conversions {}

# explore: dev_hulu_test_carbon_purchase_stream {}

# explore: dev_hulu_test_concatenated_streams {}

# explore: dev_hulu_test_find_valid_measurement_group_ids {}

# explore: dev_hulu_test_impression_stream {}

# explore: dev_hulu_test_imprstream_impressions {}

# explore: dev_hulu_test_purchases_table {}

# explore: dev_hulu_test_referral_stream {}

# explore: dev_hulu_test_roktml_impressions {}

# explore: dev_hulu_test_roktml_treatment_stream {}

# explore: dev_travelocity_gp_2019_full_fact_incrementality {}

# explore: dev_travelocity_gp_2019_full_fact_incrementality_backup {}

# explore: device_rollup {}

# explore: etag_issues_accounts {}

# explore: expedia_incrementality_conversions_20180401_20190721 {}

# explore: fraudassets_ccbin_database_201806_201904 {}

# explore: groupon_history {}

# explore: groupon_incrementality_conversions_20180312_20190325 {}

# explore: hcom_conversions_20190101_20200131 {}

# explore: hcom_conversions_20190101_20200229 {}

# explore: hotwire_conversions_20190101_20190630_backup {}

# explore: hotwire_raw_conversion_file_20190101_20190601 {}

# explore: hotwire_raw_conversion_file_20190101_20190630 {}

# explore: hotwire_suppressions_before_incrementality {}

# explore: impression {}

# explore: impressionplacement {}

# explore: incrementality_campaigns {}

# explore: kayo_conversions_191024_191028 {}

# explore: kayo_conversions_delta_20191024_20191028 {}

# explore: metrics_activity_per_placement_engagement {}

# explore: metrics_activity_per_placement_engagement_v2 {}

# explore: metrics_conversion_rate_2 {}

# explore: metrics_conversion_rate_24h {}

# explore: metrics_conversion_rate_24h_v2 {}

# explore: metrics_conversion_rate_30d {}

# explore: metrics_conversion_rate_30d_v2 {}

# explore: metrics_load_time {}

# explore: metrics_load_time_30 {}

# explore: metrics_load_time_detail {}

# explore: metrics_master {}

# explore: metrics_page_fire_rate_ {}

# explore: metrics_page_fire_rate_v2 {}

# explore: metrics_placement_eligibility {}

# explore: metrics_placement_engagement_rate {}

# explore: metrics_placement_engagement_rate2 {}

# explore: metrics_placement_engagement_rate_v2 {}

# explore: metrics_placement_impression_rate {}

# explore: metrics_placement_impression_rate_v2 {}

# explore: missing_hulu_hashes {}

# explore: nw_emails_mb {}

# explore: nw_emailsage_mb {}

# explore: nw_emailsgender_mb {}

# explore: old_world_ghost_impressions {}

# explore: orbitz_conversion_file_20190503_20190630 {}

# explore: orbitz_incrementality_gbv_conversions_20190825_20191211 {}

# explore: orbitz_incrementality_gp_conversions_20190101_20200201 {}

# explore: orbitz_incrementality_gp_conversions_20190825_20191211 {}

# explore: orbitz_incrementality_gp_hc_conversions_20190825_20191211 {}

# explore: orbitz_incrementality_gp_lc_conversions_20190825_20191211 {}

# explore: ow_emails_mb {}

# explore: ow_emailsage_mb {}

# explore: ow_emailsgender_mb {}

# explore: pinchme {}

# explore: pinchme_history {
#   join: pinchme {
#     type: left_outer
#     sql_on: ${pinchme_history.pinchmeid} = ${pinchme.pinchmeid} ;;
#     relationship: many_to_one
#   }
# }

# explore: placement_ineligibility_reasons {}

# explore: pricedreferral {}

# explore: prod_blue_apron_fact_incrementality {}

# explore: prod_blue_apron_pre_5_fact_incrementality {}

# explore: prod_dominos_au_add_consumeremailid {}

# explore: prod_dominos_au_fact_incrementality {}

# explore: prod_dominos_us_add_consumeremailid {}

# explore: prod_dominos_us_fact_incrementality {}

# explore: prod_dominos_us_fact_incrementality_backup {}

# explore: prod_grubhub_add_consumeremailid {}

# explore: prod_grubhub_fact_incrementality {}

# explore: prod_hello_fresh_fact_incrementality {}

# explore: prod_hello_fresh_fact_incrementality_backup {}

# explore: prod_hello_fresh_fill_in_mg {}

# explore: prod_hello_fresh_preprocess_rollup {}

# explore: prod_home_chef_add_consumeremailid {}

# explore: prod_home_chef_fact_incrementality {}

# explore: prod_home_chef_fill_in_mg {}

# explore: prod_home_chef_preprocess_rollup {}

# explore: prod_hotels_2019_full_fact_incrementality {}

# explore: prod_hotels_2019_full_fact_incrementality_backup {}

# explore: prod_hotels_add_audience_history {}

# explore: prod_hotels_add_consumeremailid {}

# explore: prod_hotels_add_days_between_groups {}

# explore: prod_hotels_add_days_since_last_event {}

# explore: prod_hotels_add_emailhash {}

# explore: prod_hotels_add_exists_in_purchase_stream {}

# explore: prod_hotels_add_last_seen_on_account {}

# explore: prod_hotels_add_last_wouldhaveseen_on_account {}

# explore: prod_hotels_add_row_id {}

# explore: prod_hotels_csv_freeze_jan_fact_incrementality {}

# explore: prod_hotels_csv_freeze_jan_fact_incrementality_backup {}

# explore: prod_hotels_delete_previously_exposed_to_account {}

# explore: prod_hotels_exclude150days_fact_incrementality {}

# explore: prod_hotels_exclude150days_fact_incrementality_backup {}

# explore: prod_hotels_find_valid_measurement_group_ids {}

# explore: prod_hotels_freeze_fact_incrementality {}

# explore: prod_hotels_freeze_fact_incrementality_backup {}

# explore: prod_hotels_freeze_feb_fact_incrementality {}

# explore: prod_hotels_freeze_feb_fact_incrementality_backup {}

# explore: prod_hotels_freeze_jan_fact_incrementality {}

# explore: prod_hotels_freeze_jan_fact_incrementality_backup {}

# explore: prod_hotels_freeze_mar_fact_incrementality {}

# explore: prod_hotels_freeze_mar_fact_incrementality_backup {}

# explore: prod_hotels_light_fact_incrementality {}

# explore: prod_hotels_nofixes_add_consumeremailid {}

# explore: prod_hotels_nofixes_fact_incrementality {}

# explore: prod_hotels_nofixes_fact_incrementality_backup {}

# explore: prod_hotels_nofixes_fact_incrementality_rdn {}

# explore: prod_hotels_nofixes_fact_incrementality_ux_experiment {}

# explore: prod_hotels_nofixes_pre51bleed_fact_incrementality {}

# explore: prod_hotels_nofixes_test_add_uploaded_conversions {}

# explore: prod_hotels_raw_dedupconv_add_uploaded_conversions {}

# explore: prod_hotels_raw_dedupconv_purchases_table {}

# explore: prod_hotels_rokt_impressions_union {}

# explore: prod_hotels_rop_150_add_uploaded_conversions {}

# explore: prod_hotels_rop_150_fact_incrementality {}

# explore: prod_hotels_rop_150_purchases_table {}

# explore: prod_hotels_should_have_been_suppressed {}

# explore: prod_hotels_sim_repeat_offer_penalty {}

# explore: prod_hotels_supp_add_uploaded_conversions {}

# explore: prod_hotels_supp_fact_incrementality {}

# explore: prod_hotels_supp_purchases_table {}

# explore: prod_hotels_supp_rop_150_add_uploaded_conversions {}

# explore: prod_hotels_supp_rop_150_fact_incrementality {}

# explore: prod_hotels_supp_rop_150_purchases_table {}

# explore: prod_hotels_tm_attributes {}

# explore: prod_hotels_wotif_data_cleanup {}

# explore: prod_hotwire_add_consumeremailid {}

# explore: prod_hotwire_fact_incrementality {}

# explore: prod_hotwire_roktml_referrals {}

# explore: prod_hotwire_supp_fact_incrementality {}

# explore: prod_hotwire_supp_fact_incrementality_backup {}

# explore: prod_hulu_10_fact_incrementality {}

# explore: prod_hulu_10_fact_incrementality_backup {}

# explore: prod_hulu_15_fact_incrementality {}

# explore: prod_hulu_15_fact_incrementality_backup {}

# explore: prod_hulu_28_fact_incrementality {}

# explore: prod_hulu_28_fact_incrementality_backup {}

# explore: prod_hulu_30_fact_incrementality {}

# explore: prod_hulu_7_fact_incrementality {}

# explore: prod_hulu_7_fact_incrementality_backup {}

# explore: prod_hulu_fact_incrementality {}

# explore: prod_hulu_fact_incrementality_backup {}

# explore: prod_hulu_nofixes_fact_incrementality {}

# explore: prod_kayo_add_consumeremailid {}

# explore: prod_kayo_fact_incrementality {}

# explore: prod_my_deal_add_consumeremailid {}

# explore: prod_my_deal_fact_incrementality {}

# explore: prod_my_deal_roktml_referrals {}

# explore: prod_orbitz_fact_incrementality {}

# explore: prod_orbitz_gbv_201912_fact_incrementality {}

# explore: prod_orbitz_gbv_201912_fact_incrementality_backup {}

# explore: prod_orbitz_gbv_fact_incrementality {}

# explore: prod_orbitz_gbv_fact_incrementality_backup {}

# explore: prod_orbitz_gp_201908_fact_incrementality_backup {}

# explore: prod_orbitz_gp_201908_find_valid_measurement_group_ids {}

# explore: prod_orbitz_gp_201908_purchases_table {}

# explore: prod_orbitz_gp_201912_fact_incrementality_backup {}

# explore: prod_orbitz_gp_201912_find_valid_measurement_group_ids {}

# explore: prod_orbitz_gp_201912_purchases_table {}

# explore: prod_orbitz_gp_2019_full_fact_incrementality_backup {}

# explore: prod_orbitz_gp_fact_incrementality {}

# explore: prod_orbitz_gp_fact_incrementality_backup {}

# explore: prod_orbitz_gp_hc_201912_fact_incrementality {}

# explore: prod_orbitz_gp_hc_201912_fact_incrementality_backup {}

# explore: prod_orbitz_gp_lc_201912_fact_incrementality {}

# explore: prod_orbitz_gp_lc_201912_fact_incrementality_backup {}

# explore: prod_orbitztag_fact_incrementality {}

# explore: prod_priceline_fact_incrementality {}

# explore: prod_priceline_fact_incrementality_backup {}

# explore: prod_priceline_pre_5_add_audience_history {}

# explore: prod_priceline_pre_5_add_consumeremailid {}

# explore: prod_priceline_pre_5_add_days_between_groups {}

# explore: prod_priceline_pre_5_add_days_since_last_event {}

# explore: prod_priceline_pre_5_add_emailhash {}

# explore: prod_priceline_pre_5_add_exists_in_purchase_stream {}

# explore: prod_priceline_pre_5_add_last_seen_on_account {}

# explore: prod_priceline_pre_5_add_last_wouldhaveseen_on_account {}

# explore: prod_priceline_pre_5_add_lasttouchpoint {}

# explore: prod_priceline_pre_5_add_reporting_columns {}

# explore: prod_priceline_pre_5_add_row_id {}

# explore: prod_priceline_pre_5_add_timedelta_gi_stream {}

# explore: prod_priceline_pre_5_add_uploaded_conversions {}

# explore: prod_priceline_pre_5_carbon_purchase_stream {}

# explore: prod_priceline_pre_5_concatenated_streams {}

# explore: prod_priceline_pre_5_conversion_attribution_initial_join {}

# explore: prod_priceline_pre_5_conversion_attribution_matchrank {}

# explore: prod_priceline_pre_5_conversion_attribution_stream {}

# explore: prod_priceline_pre_5_delete_previously_exposed_to_account {}

# explore: prod_priceline_pre_5_fact_incrementality {}

# explore: prod_priceline_pre_5_fact_incrementality_backup {}

# explore: prod_priceline_pre_5_find_bleed_windows {}

# explore: prod_priceline_pre_5_find_valid_measurement_group_ids {}

# explore: prod_priceline_pre_5_flag_impressions_with_referrals {}

# explore: prod_priceline_pre_5_flag_timeouts {}

# explore: prod_priceline_pre_5_flag_userpath_impressions {}

# explore: prod_priceline_pre_5_flag_valid_ghost_impressions {}

# explore: prod_priceline_pre_5_ghost_impression_stream {}

# explore: prod_priceline_pre_5_impression_stream {}

# explore: prod_priceline_pre_5_imprstream_impressions {}

# explore: prod_priceline_pre_5_mark_customer_bleed {}

# explore: prod_priceline_pre_5_mark_user_bleed {}

# explore: prod_priceline_pre_5_mark_user_outlier {}

# explore: prod_priceline_pre_5_preprocess_table {}

# explore: prod_priceline_pre_5_purchases_table {}

# explore: prod_priceline_pre_5_referral_stream {}

# explore: prod_priceline_pre_5_rokt_impressions_union {}

# explore: prod_priceline_pre_5_roktml_impressions {}

# explore: prod_priceline_pre_5_roktml_referrals {}

# explore: prod_priceline_pre_5_roktml_treatment_stream {}

# explore: prod_priceline_pre_5_should_have_been_suppressed {}

# explore: prod_priceline_pre_5_sim_repeat_offer_penalty {}

# explore: prod_priceline_pre_5_tm_attributes {}

# explore: prod_priceline_pre_5_wotif_data_cleanup {}

# explore: prod_priceline_pre_oct_fact_incrementality {}

# explore: prod_priceline_pre_oct_fact_incrementality_backup {}

# explore: prod_quip_add_consumeremailid {}

# explore: prod_quip_fact_incrementality {}

# explore: prod_siriusxm_fact_incrementality {}

# explore: prod_spirit_fact_incrementality {}

# explore: prod_stubhub_add_consumeremailid {}

# explore: prod_stubhub_fact_incrementality {}

# explore: prod_tomorro_add_audience_history {}

# explore: prod_tomorro_add_consumeremailid {}

# explore: prod_tomorro_add_emailhash {}

# explore: prod_tomorro_add_exists_in_purchase_stream {}

# explore: prod_tomorro_add_last_seen_on_account {}

# explore: prod_tomorro_add_row_id {}

# explore: prod_tomorro_add_timedelta_gi_stream {}

# explore: prod_tomorro_add_uploaded_conversions {}

# explore: prod_tomorro_carbon_purchase_stream {}

# explore: prod_tomorro_concatenated_streams {}

# explore: prod_tomorro_delete_previously_exposed_to_account {}

# explore: prod_tomorro_fact_incrementality {}

# explore: prod_tomorro_fact_incrementality_backup {}

# explore: prod_tomorro_find_valid_measurement_group_ids {}

# explore: prod_tomorro_flag_timeouts {}

# explore: prod_tomorro_flag_userpath_impressions {}

# explore: prod_tomorro_flag_valid_ghost_impressions {}

# explore: prod_tomorro_ghost_impression_stream {}

# explore: prod_tomorro_ghost_impressions_union {}

# explore: prod_tomorro_impression_stream {}

# explore: prod_tomorro_imprstream_impressions {}

# explore: prod_tomorro_purchases_table {}

# explore: prod_tomorro_rdn_ghost_impressions {}

# explore: prod_tomorro_referral_stream {}

# explore: prod_tomorro_rokt_impressions_union {}

# explore: prod_tomorro_roktml_impressions {}

# explore: prod_tomorro_roktml_referrals {}

# explore: prod_tomorro_roktml_treatment_stream {}

# explore: prod_tomorro_should_have_been_suppressed {}

# explore: prod_tomorro_sim_repeat_offer_penalty {}

# explore: prod_tomorro_tm_attributes {}

# explore: prod_tomorro_wotif_data_cleanup {}

# explore: prod_travelocity_add_audience_history {}

# explore: prod_travelocity_add_consumeremailid {}

# explore: prod_travelocity_add_emailhash {}

# explore: prod_travelocity_add_exists_in_purchase_stream {}

# explore: prod_travelocity_add_last_seen_on_account {}

# explore: prod_travelocity_add_row_id {}

# explore: prod_travelocity_add_timedelta_gi_stream {}

# explore: prod_travelocity_add_uploaded_conversions {}

# explore: prod_travelocity_carbon_purchase_stream {}

# explore: prod_travelocity_concatenated_streams {}

# explore: prod_travelocity_fact_incrementality {}

# explore: prod_travelocity_fact_incrementality_backup {}

# explore: prod_travelocity_find_valid_measurement_group_ids {}

# explore: prod_travelocity_flag_timeouts {}

# explore: prod_travelocity_flag_userpath_impressions {}

# explore: prod_travelocity_flag_valid_ghost_impressions {}

# explore: prod_travelocity_gbv_201909_fact_incrementality {}

# explore: prod_travelocity_gbv_201909_fact_incrementality_backup {}

# explore: prod_travelocity_gbv_201912_fact_incrementality {}

# explore: prod_travelocity_gbv_201912_fact_incrementality_backup {}

# explore: prod_travelocity_gbv_201912_find_valid_measurement_group_ids {}

# explore: prod_travelocity_gbv_fact_incrementality {}

# explore: prod_travelocity_gbv_fact_incrementality_backup {}

# explore: prod_travelocity_ghost_impression_stream {}

# explore: prod_travelocity_ghost_impressions_union {}

# explore: prod_travelocity_gp_201908_fact_incrementality {}

# explore: prod_travelocity_gp_201908_fact_incrementality_backup {}

# explore: prod_travelocity_gp_201909_fact_incrementality {}

# explore: prod_travelocity_gp_201909_fact_incrementality_backup {}

# explore: prod_travelocity_gp_201912_fact_incrementality {}

# explore: prod_travelocity_gp_201912_fact_incrementality_backup {}

# explore: prod_travelocity_gp_2019_full_fact_incrementality {}

# explore: prod_travelocity_gp_2019_full_fact_incrementality_backup {}

# explore: prod_travelocity_gp_fact_incrementality {}

# explore: prod_travelocity_gp_fact_incrementality_backup {}

# explore: prod_travelocity_gp_hc_201912_fact_incrementality {}

# explore: prod_travelocity_gp_hc_201912_fact_incrementality_backup {}

# explore: prod_travelocity_gp_lc_201912_fact_incrementality {}

# explore: prod_travelocity_gp_lc_201912_fact_incrementality_backup {}

# explore: prod_travelocity_impression_stream {}

# explore: prod_travelocity_imprstream_impressions {}

# explore: prod_travelocity_purchases_table {}

# explore: prod_travelocity_rdn_ghost_impressions {}

# explore: prod_travelocity_referral_stream {}

# explore: prod_travelocity_rokt_impressions_union {}

# explore: prod_travelocity_roktml_impressions {}

# explore: prod_travelocity_roktml_referrals {}

# explore: prod_travelocity_roktml_treatment_stream {}

# explore: prod_travelocity_should_have_been_suppressed {}

# explore: prod_travelocity_sim_repeat_offer_penalty {}

# explore: prod_travelocity_tm_attributes {}

# explore: prod_travelocity_wotif_data_cleanup {}

# explore: prod_wayfair_fact_incrementality_backup {}

# explore: prod_wayfair_roktml_referrals {}

# explore: prod_wotif_fact_incrementality {}

# explore: prod_wotif_gbv_201912_fact_incrementality {}

# explore: prod_wotif_gbv_201912_fact_incrementality_backup {}

# explore: prod_wotif_gbv_fact_incrementality {}

# explore: prod_wotif_gbv_fact_incrementality_backup {}

# explore: prod_wotif_gp_201908_fact_incrementality {}

# explore: prod_wotif_gp_201908_fact_incrementality_backup {}

# explore: prod_wotif_gp_201908_find_valid_measurement_group_ids {}

# explore: prod_wotif_gp_201908_purchases_table {}

# explore: prod_wotif_gp_201912_fact_incrementality {}

# explore: prod_wotif_gp_201912_fact_incrementality_backup {}

# explore: prod_wotif_gp_201912_find_valid_measurement_group_ids {}

# explore: prod_wotif_gp_201912_purchases_table {}

# explore: prod_wotif_gp_2019_full_fact_incrementality {}

# explore: prod_wotif_gp_2019_full_fact_incrementality_backup {}

# explore: prod_wotif_gp_fact_incrementality {}

# explore: prod_wotif_gp_fact_incrementality_backup {}

# explore: prod_wotif_gp_hc_201912_fact_incrementality {}

# explore: prod_wotif_gp_hc_201912_fact_incrementality_backup {}

# explore: prod_wotif_gp_lc_201912_fact_incrementality {}

# explore: prod_wotif_gp_lc_201912_fact_incrementality_backup {}

# explore: prod_zanui_fact_incrementality {}

# explore: product_category_margins {}

# explore: raw_comet_conversion_file_20200106 {}

# explore: raw_comet_conversion_file_20200213 {}

# explore: raw_conversions_hcom_frnl_20190417 {}

# explore: raw_hcom_conversion_file_20200213 {}

# explore: raw_hcom_conversion_file_20200327 {}

# explore: raw_hulu_conversion_file_2020004 {}

# explore: raw_orbitz_conversion_file_20190829 {}

# explore: raw_travelocity_conversion_file_20200106 {}

# explore: raw_wotif_conversion_file_20190829 {}

# explore: reportableattributedconversion {}

# explore: reporting_metrics_ts {}

# explore: rokt_metrics {}

# explore: rokt_metrics_conversion_rate {}

# explore: rokt_placement_ineligibility_metrics {}

# explore: rokt_placement_ineligibility_metrics_backup {}

# explore: rokt_placement_ineligibility_reasons {}

# explore: rokt_placement_names {}

# explore: rokt_referral_traffic_metrics {}

# explore: rokt_referral_traffic_metrics_backup {}

# explore: rokt_reporting_campaign_metrics {}

# explore: rokt_reporting_campaign_metrics_2 {}

# explore: rokt_reporting_campaign_metrics_backup {}

# explore: rokt_reporting_metrics {}

# explore: rokt_reporting_metrics_backup {}

# explore: rokt_reporting_metrics_detail {}

# explore: rokt_reporting_metrics_moloy {}

# explore: rokt_reporting_metrics_parent_accounts {}

# explore: roktmlbidding {}

# explore: selectednoplacement {}

# explore: selectedplacement {}

# explore: session_mobilesha256 {}

# explore: spirit_bfs_conversions_20190415_20190615 {}

# explore: spirit_conversions_20190415_20190615 {}

# explore: spirit_conversions_cleaned_20190415_20190615 {}

# explore: spirit_nfc_conversions_20190415_20190615 {}

# explore: spirit_pnr_codes {}

# explore: ss_load_time {}

# explore: subroktmlauctioncreatives {}

# explore: tableau_template_extract {}

# explore: tagfire {}

# explore: tagfire_incremental {}

# explore: tagfire_rollup {}

# explore: test_blue_apron_fact_incrementality {}

# explore: toby_hulu_nofixes_fact_incrementality {}

# explore: tomorro_audience_pool {}

# explore: tomorro_audience_pool_split {}

# explore: traffic_metrics_last_updated {}

# explore: travelocity_conversions_by_trl {}

# explore: travelocity_incrementality_gbv_conversions_20190825_20191211 {}

# explore: travelocity_incrementality_gbv_conversions_20190829 {}

# explore: travelocity_incrementality_gp_conversions_20190101_20200201 {}

# explore: travelocity_incrementality_gp_conversions_20190825_20191211 {}

# explore: travelocity_incrementality_gp_conversions_20190829 {}

# explore: travelocity_incrementality_gp_hc_conversions_20190825_20191211 {}

# explore: travelocity_incrementality_gp_lc_conversions_20190825_20191211 {}

# explore: uploadedconversion {}

# explore: userlist {}

# explore: v_ghost_impression_stream {}

# explore: verifiedreferral {}

# explore: widget_url_not_opened_logs {}

# explore: wotif_exposed_users_20190801 {}

# explore: wotif_exposed_users_20190801_old {}

# explore: wotif_incrementality_gbv_conversions_20190825_20191211 {}

# explore: wotif_incrementality_gp_conversions_20190101_20200201 {}

# explore: wotif_incrementality_gp_conversions_20190825_20191211 {}

# explore: wotif_incrementality_gp_hc_conversions_20190825_20191211 {}

# explore: wotif_incrementality_gp_lc_conversions_20190825_20191211 {}

